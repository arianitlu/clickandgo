package arianit.com.clickandgo.model;

public class Business {

    String emri;
    String mbiemri;
    String adresa;
    String telefoni;
    String email;
    Post post;

    public Business(String emri, String mbiemri, String adresa, String telefoni, String email, Post post) {
        this.emri = emri;
        this.mbiemri = mbiemri;
        this.adresa = adresa;
        this.telefoni = telefoni;
        this.email = email;
        this.post = post;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getMbiemri() {
        return mbiemri;
    }

    public void setMbiemri(String mbiemri) {
        this.mbiemri = mbiemri;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getTelefoni() {
        return telefoni;
    }

    public void setTelefoni(String telefoni) {
        this.telefoni = telefoni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
