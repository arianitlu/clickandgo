package arianit.com.clickandgo.model;

import java.util.List;

public class Post {

    int id;
    String titulli;
    String nentitulli;
    int tipi;
    int zbritja;
    String dataPrej;
    String dataDeri;
    String pershkrimi;

    public Post(String titulli, String dataPrej, String pershkrimi, int foto, int logo) {
        this.titulli = titulli;
        this.dataPrej = dataPrej;
        this.pershkrimi = pershkrimi;
        this.foto = foto;
        this.logo = logo;
    }

    // change to string when you connect with firebase
    int foto;
    int logo;

    List<String> fotot;

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulli() {
        return titulli;
    }

    public void setTitulli(String titulli) {
        this.titulli = titulli;
    }

    public String getNentitulli() {
        return nentitulli;
    }

    public void setNentitulli(String nentitulli) {
        this.nentitulli = nentitulli;
    }

    public int getTipi() {
        return tipi;
    }

    public void setTipi(int tipi) {
        this.tipi = tipi;
    }

    public int getZbritja() {
        return zbritja;
    }

    public void setZbritja(int zbritja) {
        this.zbritja = zbritja;
    }

    public String getDataPrej() {
        return dataPrej;
    }

    public void setDataPrej(String dataPrej) {
        this.dataPrej = dataPrej;
    }

    public String getDataDeri() {
        return dataDeri;
    }

    public void setDataDeri(String dataDeri) {
        this.dataDeri = dataDeri;
    }

    public String getPershkrimi() {
        return pershkrimi;
    }

    public void setPershkrimi(String pershkrimi) {
        this.pershkrimi = pershkrimi;
    }

    public List<String> getFotot() {
        return fotot;
    }

    public void setFotot(List<String> fotot) {
        this.fotot = fotot;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", titulli='" + titulli + '\'' +
                ", nentitulli='" + nentitulli + '\'' +
                ", tipi=" + tipi +
                ", zbritja=" + zbritja +
                ", dataPrej='" + dataPrej + '\'' +
                ", dataDeri='" + dataDeri + '\'' +
                ", pershkrimi='" + pershkrimi + '\'' +
                ", foto=" + foto +
                ", logo=" + logo +
                ", fotot=" + fotot +
                '}';
    }
}
