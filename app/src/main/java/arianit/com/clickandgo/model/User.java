package arianit.com.clickandgo.model;

public class User {

    String emri_mbiemri_pronarit;
    String emri_biznesit;
    String username;
    String email;
    String lloji_biznesit;
    String lokacioni;
    String website;
    String numri_tel;

    public User() {
    }

    public User(String emri_mbiemri_pronarit, String emri_biznesit, String username, String email,
                String lloji_biznesit, String lokacioni, String website, String numri_tel) {
        this.emri_mbiemri_pronarit = emri_mbiemri_pronarit;
        this.emri_biznesit = emri_biznesit;
        this.username = username;
        this.email = email;
        this.lloji_biznesit = lloji_biznesit;
        this.lokacioni = lokacioni;
        this.website = website;
        this.numri_tel = numri_tel;
    }

    public String getEmri_mbiemri_pronarit() {
        return emri_mbiemri_pronarit;
    }

    public void setEmri_mbiemri_pronarit(String emri_mbiemri_pronarit) {
        this.emri_mbiemri_pronarit = emri_mbiemri_pronarit;
    }

    public String getEmri_biznesit() {
        return emri_biznesit;
    }

    public void setEmri_biznesit(String emri_biznesit) {
        this.emri_biznesit = emri_biznesit;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLloji_biznesit() {
        return lloji_biznesit;
    }

    public void setLloji_biznesit(String lloji_biznesit) {
        this.lloji_biznesit = lloji_biznesit;
    }

    public String getLokacioni() {
        return lokacioni;
    }

    public void setLokacioni(String lokacioni) {
        this.lokacioni = lokacioni;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getNumri_tel() {
        return numri_tel;
    }

    public void setNumri_tel(String numri_tel) {
        this.numri_tel = numri_tel;
    }
}
