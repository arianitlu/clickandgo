package arianit.com.clickandgo;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import arianit.com.clickandgo.fragments.EventeFragment;
import arianit.com.clickandgo.fragments.HomeFragment;
import arianit.com.clickandgo.fragments.OfertaFragment;
import arianit.com.clickandgo.fragments.RisiFragment;
import arianit.com.clickandgo.fragments.ShtoFragment;

public class MainActivity extends AppCompatActivity {

    public static final int POSITION_HOME = 0;
    public static final int POSITION_OFERTA = 1;
    public static final int POSITION_SHTO = 2;
    public static final int POSITION_RISI = 3;
    public static final int POSITION_EVENTE = 4;

    BottomNavigationViewEx bottomNavigationViewEx;

    ImageView img_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            w.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        img_search = findViewById(R.id.img_search);

        setUpBottomNavigationView();


    }

    public void changeIcon(){
        img_search.setImageResource(R.drawable.ic_check);
    }

    private void setUpBottomNavigationView() {
        bottomNavigationViewEx = findViewById(R.id.bottom_nav);
        bottomNavigationViewEx.enableAnimation(true);
        bottomNavigationViewEx.enableShiftingMode(true);
        bottomNavigationViewEx.enableItemShiftingMode(true);

        switchFragment(POSITION_HOME);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.nav_home:
                                switchFragment(POSITION_HOME);
                                return true;
                            case R.id.nav_oferta:
                                switchFragment(POSITION_OFERTA);
                                return true;
                            case R.id.nav_add:
                                switchFragment(POSITION_SHTO);
                                return true;
                            case R.id.nav_risi:
                                switchFragment(POSITION_RISI);
                                return true;
                            case R.id.nav_evente:
                                switchFragment(POSITION_EVENTE);
                                return true;
                        }
                        return false;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        bottomNavigationViewEx = findViewById(R.id.bottom_nav);
        if (bottomNavigationViewEx.getSelectedItemId() == R.id.nav_home)
        {
            super.onBackPressed();
            finish();
        }
        else
        {
            bottomNavigationViewEx.setSelectedItemId(R.id.nav_home);
        }
    }

    public void switchFragment(int position) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_frame_layou, getFragment(position), getFragmentTitle(position))
                .commit();
    }

    private Fragment getFragment(int position) {

        switch (position) {
            case POSITION_HOME:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case POSITION_OFERTA:
                OfertaFragment ofertaFragment = new OfertaFragment();
                return ofertaFragment;
            case POSITION_SHTO:
                ShtoFragment shtoFragment = new ShtoFragment();
                return shtoFragment;
            case POSITION_RISI:
                RisiFragment risiFragment = new RisiFragment();
                return risiFragment;
            case POSITION_EVENTE:
                EventeFragment eventeFragment = new EventeFragment();
                return eventeFragment;
            default:
                return null;
        }
    }

    private String getFragmentTitle(int position) {
        switch (position) {
            case POSITION_HOME:
                return getString(R.string.main_nav_home);
            case POSITION_OFERTA:
                return getString(R.string.main_nav_oferta);
            case POSITION_SHTO:
                return getString(R.string.main_nav_shto);
            case POSITION_RISI:
                return getString(R.string.main_nav_risi);
            case POSITION_EVENTE:
                return getString(R.string.main_nav_evente);
            default:
                return null;
        }
    }

}
