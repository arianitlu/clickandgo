package arianit.com.clickandgo;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import arianit.com.clickandgo.model.User;

public class RegisterBussinesActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference noteRef = db.collection("Users");

    TextView emri_mbiemri_pronarit;
    TextView emri_biznesit;
   // TextView username;
    TextView email;
    TextView lloji_biznesit;
    TextView lokacioni;
    TextView website;
    TextView numri_tel;

    Button btnRegjistrohu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_bussines);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        emri_mbiemri_pronarit = findViewById(R.id.txt_emri_pronarit);
        emri_biznesit = findViewById(R.id.txt_emri_biznesit);
        //username = findViewById(R.id.txt_username);
        email = findViewById(R.id.txt_email);
        lloji_biznesit = findViewById(R.id.txt_lloji_biznesit);
        lokacioni = findViewById(R.id.txt_lokacioni);
        website = findViewById(R.id.txt_website);
        numri_tel = findViewById(R.id.txt_numri_telefonit);

        btnRegjistrohu = findViewById(R.id.txt_regjistrohu);

        btnRegjistrohu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User(
                        emri_mbiemri_pronarit.getText().toString() + "",
                        emri_biznesit.getText().toString()+ "",
                        "",
                        email.getText().toString()+ "",
                        lloji_biznesit.getText().toString()+ "",
                        lokacioni.getText().toString()+ "",
                        website.getText().toString()+ "",
                        numri_tel.getText().toString()+ "");

                writeNewUser(user);
            }
        });

    }

    private void writeNewUser(User user) {

            noteRef.document()
                .set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(RegisterBussinesActivity.this,
                            "U regjistrua me sukses nje biznes i ri!", Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(RegisterBussinesActivity.this,
                            "Gabim gjate procesit te regjistrimit!", Toast.LENGTH_LONG).show();
                }
            });

    }
}
