package arianit.com.clickandgo.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import arianit.com.clickandgo.DetailActivity;
import arianit.com.clickandgo.R;
import arianit.com.clickandgo.model.Post;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    private List<Post> mPostsList;
    private Context ctx;

    public PostAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mTitulli, mPershkrimi, mData;
        ImageView mImage, mLogo;

        public MyViewHolder(View itemView) {
            super(itemView);

            mTitulli = itemView.findViewById(R.id.txt_titulli);
            mPershkrimi = itemView.findViewById(R.id.txt_pershkrimi);
            mData = itemView.findViewById(R.id.txt_date);

            mImage = itemView.findViewById(R.id.img_photo);
            mLogo = itemView.findViewById(R.id.img_logo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, DetailActivity.class);
                    intent.putExtra("titulli",mPostsList.get(getAdapterPosition()).getTitulli());
                    ctx.startActivity(intent);
                }
            });
        }
    }

    @Override
    public PostAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostAdapter.MyViewHolder holder, int position) {
        Post post = mPostsList.get(position);

        holder.mTitulli.setText(post.getTitulli());
        holder.mPershkrimi.setText(post.getPershkrimi());
        holder.mData.setText(post.getDataPrej());

        holder.mImage.setImageResource(post.getFoto());
        holder.mLogo.setImageResource(post.getLogo());

//        Picasso.with(ctx)
//                .load(list.getImg())
//                .into(holder.mImageViewMain);
    }

    public void setPost(List<Post> postsList){

        this.mPostsList = postsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mPostsList.size();
    }
}