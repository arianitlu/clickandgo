package arianit.com.clickandgo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class LoginOptionsActivity extends AppCompatActivity {

    ImageView registerAsBussiness, registerAsIndividual;
    ImageView loginIndividual, loginBussiness;

    TextView txtSiMusafir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_options);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        registerAsBussiness = findViewById(R.id.register_bussiness);
        registerAsIndividual = findViewById(R.id.register_individ);

        loginIndividual = findViewById(R.id.img_individ);
        loginBussiness = findViewById(R.id.img_bussiness);

        txtSiMusafir = findViewById(R.id.txt_si_musafir);

        loginIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginOptionsActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        loginBussiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginOptionsActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        registerAsBussiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginOptionsActivity.this,RegisterBussinesActivity.class);
                startActivity(intent);
            }
        });

        registerAsIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginOptionsActivity.this,RegisterIndividualActivity.class);
                startActivity(intent);
            }
        });

        txtSiMusafir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginOptionsActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }

}
