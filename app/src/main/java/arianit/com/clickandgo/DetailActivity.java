package arianit.com.clickandgo;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import arianit.com.clickandgo.adapters.MyCustomPagerAdapter;

public class DetailActivity extends AppCompatActivity {

    TextView txtName, txtTel, txtWeb, txtMap;

    private LinearLayout dots_layout;
    private ImageView[] dots;
    private ViewPager viewPager;
    private int[] layouts = {R.drawable.oferta1a, R.drawable.oferta2a, R.drawable.news1c};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_detail);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int images[] = {R.drawable.oferta1a, R.drawable.oferta1b, R.drawable.news1a};

        txtName = findViewById(R.id.txt_name);
        txtTel = findViewById(R.id.txt_tel);
        txtWeb = findViewById(R.id.txt_web);
        txtMap = findViewById(R.id.textView9);

        txtTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = "tel:+377 45 669 850";
                onCall(tel);
            }
        });

        txtWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://www.example.com");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        txtMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://maps.google.co.in/maps?q=" + "Prizren"));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        Intent intent = getIntent();

        String titulli = intent.getStringExtra("titulli");

        txtName.setText(titulli);

        MyCustomPagerAdapter myCustomPagerAdapter;
        viewPager = findViewById(R.id.viewpager_details);

        myCustomPagerAdapter = new MyCustomPagerAdapter(DetailActivity.this, images);
        viewPager.setAdapter(myCustomPagerAdapter);

        dots_layout = findViewById(R.id.dotsLayout);

        createDots(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void createDots(int current_position){

        if (dots_layout != null)
            dots_layout.removeAllViews();

        dots = new ImageView[layouts.length];

        for (int i=0 ; i<layouts.length ; i++){
            dots[i] = new ImageView(this);

            if (i==current_position){
                dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.active_dots));
            }
            else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.active_dots_arch));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4,0,10,0);

            dots_layout.addView(dots[i],params);
        }

    }

    public void onCall(String tel) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(tel));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    10);
            return;
        }else {
            try{
                startActivity(callIntent);
            }
            catch (android.content.ActivityNotFoundException ex){
                Toast.makeText(getApplicationContext(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
