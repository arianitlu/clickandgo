package arianit.com.clickandgo.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import arianit.com.clickandgo.R;
import arianit.com.clickandgo.model.Post;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class ShtoFragment extends Fragment implements View.OnClickListener {

    boolean risiClicked = false;
    boolean eventeClicked = false;
    boolean ofertaClicked = false;

    Button btnRisi, btnEvente, btnOferte, btnSave;
    EditText txtTitulli, txtNenTitulli, txtPershkrimi;
    ImageView saveIcon, imgUploadPhotos, img_photo1, img_photo2, img_photo3;

    EditText dateFrom;
    EditText dateTo;

    List<String> mPhotos = new ArrayList<>();


    public ShtoFragment() {
    }

    public static ShtoFragment newInstance() {
        ShtoFragment fragment = new ShtoFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post, container, false);

        EasyImage.configuration(getContext())
                .setCopyExistingPicturesToPublicLocation(false)
                .saveInRootPicturesDirectory()
                .saveInRootPicturesDirectory();

        bindViews(rootView);

        setListeners();

        setUpDateDialogs();

        saveIcon.setImageResource(R.drawable.ic_check);

        return rootView;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EasyImage.clearConfiguration(getContext());
    }

    private void saveData() {
        String titulli = txtTitulli.getText().toString();
        String nenTitulli = txtNenTitulli.getText().toString();
        String pershkrimi = txtPershkrimi.getText().toString();

        String dataPrej = dateFrom.getText().toString();
        String dateDeri = dateTo.getText().toString();

        Post post = new Post(titulli, dataPrej, dateDeri, 0, 0);

        Toast.makeText(getContext(), "Post: " + post, Toast.LENGTH_SHORT).show();
    }

    private void bindViews(View view) {

        btnRisi = view.findViewById(R.id.btn_risi);
        btnEvente = view.findViewById(R.id.btn_evente);
        btnOferte = view.findViewById(R.id.btn_oferta);

        txtTitulli = view.findViewById(R.id.edt_titulli);
        txtNenTitulli = view.findViewById(R.id.edt_nen_titulli);
        txtPershkrimi = view.findViewById(R.id.edt_pershkrimi);

        dateFrom = view.findViewById(R.id.edt_data_prej);
        dateTo = view.findViewById(R.id.edt_data_deri);

        saveIcon = getActivity().findViewById(R.id.img_search);

        imgUploadPhotos = view.findViewById(R.id.img_upload_photos);

        img_photo1 = view.findViewById(R.id.image_view_1);
        img_photo2 = view.findViewById(R.id.image_view_2);
        img_photo3 = view.findViewById(R.id.image_view_3);

    }

    private void setUpDateDialogs() {
        dateFrom.setFocusable(false);
        dateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                final int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar birthDate = Calendar.getInstance();
                        birthDate.set(year, month, dayOfMonth);
                        dateFrom.setText(formatDate(birthDate.getTime(), "MMM d, yyyy"));
                        //mDate = String.format("%02d/%02d/%d", dayOfMonth, month, year);
                    }
                }, year, month, day);
                dialog.show();
            }
        });

        dateTo.setFocusable(false);
        dateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                final int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar birthDate = Calendar.getInstance();
                        birthDate.set(year, month, dayOfMonth);
                        dateTo.setText(formatDate(birthDate.getTime(), "MMM d, yyyy"));
                        //mDate = String.format("%02d/%02d/%d", dayOfMonth, month, year);
                    }
                }, year, month, day);
                dialog.show();
            }
        });
    }

    public static String formatDate(Date dateObject, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(dateObject);
    }

    private void setListeners() {
        btnRisi.setOnClickListener(this);
        btnEvente.setOnClickListener(this);
        btnOferte.setOnClickListener(this);
        saveIcon.setOnClickListener(this);
        imgUploadPhotos.setOnClickListener(this);
    }

    private void uploadPhotos() {
        EasyImage.openGallery(this,1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                String filePath1 = "";
                String filePath2 = "";
                String filePath3 = "";
                Bitmap bitmap;

                if (img_photo1.getDrawable() == null){
                    filePath1 = imageFile.getPath();
                    bitmap = BitmapFactory.decodeFile(filePath1);
                    img_photo1.setImageBitmap(bitmap);
                    mPhotos.add(filePath1);
                }else if (img_photo2.getDrawable() == null){
                    filePath2 = imageFile.getPath();
                    bitmap = BitmapFactory.decodeFile(filePath2);
                    img_photo2.setImageBitmap(bitmap);
                    mPhotos.add(filePath2);
                }else{
                    filePath3 = imageFile.getPath();
                    bitmap = BitmapFactory.decodeFile(filePath3);
                    img_photo3.setImageBitmap(bitmap);
                    mPhotos.add(filePath3);
                }

                Toast.makeText(getContext(), "Path1: " + mPhotos.get(0) + "\nPath2: " + mPhotos.get(1)
                        + "\nPath3: " + mPhotos.get(2), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                super.onImagePickerError(e, source, type);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_risi: {
                if (risiClicked) {
                    btnRisi.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    risiClicked = false;
                } else {
                    btnRisi.setBackgroundColor(getResources().getColor(R.color.colorAccent2));
                    risiClicked = true;
                }
            }
            break;

            case R.id.btn_evente: {
                if (eventeClicked) {
                    btnEvente.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    eventeClicked = false;
                } else {
                    btnEvente.setBackgroundColor(getResources().getColor(R.color.colorAccent2));
                    eventeClicked = true;
                }
            }
            break;

            case R.id.btn_oferta: {
                if (ofertaClicked) {
                    btnOferte.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    ofertaClicked = false;
                } else {
                    btnOferte.setBackgroundColor(getResources().getColor(R.color.colorAccent2));
                    ofertaClicked = true;
                }
            }
            break;

            case R.id.img_search  : {
                saveData();
            }
            break;

            case R.id.img_upload_photos  : {
                uploadPhotos();
            }
            break;

            default:
                break;
        }
    }
}
