package arianit.com.clickandgo.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import arianit.com.clickandgo.R;
import arianit.com.clickandgo.adapters.PostAdapter;
import arianit.com.clickandgo.model.Post;

public class RisiFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Post> postList = new ArrayList<>();
    PostAdapter adapter;

    public RisiFragment() {
    }

    public static RisiFragment newInstance() {
        RisiFragment fragment = new RisiFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);


        recyclerView = rootView.findViewById(R.id.recyclerview_post);

        adapter = new PostAdapter(getContext());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setPost(listPosts());

        ImageView imageView = (ImageView) getActivity().findViewById(R.id.img_search);
        imageView.setImageResource(R.drawable.ic_search);

        return rootView;

    }

    public List<Post> listPosts() {

        postList.clear();

        Post obj1 = new Post("LC WAIKIKI","30/11/2019","KOLEKCIONI I RI PRANVEROR DHE VEROR NE TE GJITHA PIKAT E LC WAIKIKI",
                R.drawable.news1a,0);
        postList.add(obj1);

        Post obj2 = new Post("LC WAIKIKI","03/12/2019","KOLEKCIONI I RI PRANVEROR DHE VEROR NE TE GJITHA PIKAT E LC WAIKIKI",
                R.drawable.news1b,0);
        postList.add(obj2);

        Post obj3 = new Post("LC WAIKIKI","14/09/2019","KOLEKCIONI I RI PRANVEROR DHE VEROR NE TE GJITHA PIKAT E LC WAIKIKI",
                R.drawable.news1c,0);
        postList.add(obj3);

        Post obj4 = new Post("01 BAU","14/1/2020","VEGLAT E REJA NGA ZVICRA DHE GJERMANIA",
                R.drawable.news2a,0);
        postList.add(obj4);

        Post obj5 = new Post("01 BAU","15/11/2019","VEGLAT E REJA NGA ZVICRA DHE GJERMANIA",
                R.drawable.news2b,0);
        postList.add(obj5);


        return postList;
    }
}
