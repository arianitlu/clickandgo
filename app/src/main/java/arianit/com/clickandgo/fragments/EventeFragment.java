package arianit.com.clickandgo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import arianit.com.clickandgo.MainActivity;
import arianit.com.clickandgo.R;
import arianit.com.clickandgo.adapters.PostAdapter;
import arianit.com.clickandgo.model.Post;

public class EventeFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Post> postList = new ArrayList<>();
    PostAdapter adapter;

    MainActivity activity;

    public EventeFragment() {
    }

    public static EventeFragment newInstance() {
        EventeFragment fragment = new EventeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        recyclerView = rootView.findViewById(R.id.recyclerview_post);

        adapter = new PostAdapter(getContext());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setPost(listPosts());

        ImageView imageView = (ImageView) getActivity().findViewById(R.id.img_search);
        imageView.setImageResource(R.drawable.ic_search);

        return rootView;

    }

    public List<Post> listPosts() {

        postList.clear();

        Post obj1 = new Post("DAFINA ZEQIRI","30/11/2019","DAFINA ZEQIRI LIVE TE MERKUREN / 06.07.2019 / 20:00",
                R.drawable.event2a,0);
        postList.add(obj1);

        Post obj2 = new Post("DAFINA ZEQIRI","02/09/2019","DAFINA ZEQIRI LIVE TE MERKUREN / 06.07.2019 / 20:00 ",
                R.drawable.event2b,0);
        postList.add(obj2);

        Post obj3 = new Post("DAFINA ZEQIRI","01/08/2019","DAFINA ZEQIRI LIVE TE MERKUREN / 06.07.2019 / 20:00",
                R.drawable.event2c,0);
        postList.add(obj3);

        Post obj4 = new Post("HAPPY HOUR`S 1+1","30/11/2019","HAPPY HOURS ATOMIC BAR MER NJE BIRE DHE NJE GRATIS PREJ ORES 16:00-19:00",
                R.drawable.event1a,0);
        postList.add(obj4);

        Post obj5 = new Post("HAPPY HOUR`S 1+1","02/09/2019","HAPPY HOURS ATOMIC BAR MER NJE BIRE DHE NJE GRATIS PREJ ORES 16:00-19:00",
                R.drawable.event1b,0);
        postList.add(obj5);

        Post obj6 = new Post("HAPPY HOUR`S 1+1","01/08/2019","HAPPY HOURS ATOMIC BAR MER NJE BIRE DHE NJE GRATIS PREJ ORES 16:00-19:00",
                R.drawable.event1c,0);
        postList.add(obj6);


        return postList;
    }
}
