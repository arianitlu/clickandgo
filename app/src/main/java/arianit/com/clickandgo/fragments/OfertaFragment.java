package arianit.com.clickandgo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import arianit.com.clickandgo.R;
import arianit.com.clickandgo.adapters.PostAdapter;
import arianit.com.clickandgo.model.Post;

public class OfertaFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Post> postList = new ArrayList<>();
    PostAdapter adapter;

    public OfertaFragment() {
    }

    public static OfertaFragment newInstance() {
        OfertaFragment fragment = new OfertaFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        recyclerView = rootView.findViewById(R.id.recyclerview_post);

        adapter = new PostAdapter(getContext());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setPost(list());

        ImageView imageView = (ImageView) getActivity().findViewById(R.id.img_search);
        imageView.setImageResource(R.drawable.ic_search);

        return rootView;

    }

    public List<Post> list() {

        postList.clear();

        Post obj1 = new Post("KFC","30/11/2019","BUY 4PC HOT&CRISPY GET 4PC STRIPS",
                R.drawable.oferta1a,0);
        postList.add(obj1);

        Post obj2 = new Post("KFC","08/11/2019","BUY 4PC HOT&CRISPY GET 4PC STRIPS",
                R.drawable.oferta1b,0);
        postList.add(obj2);

        Post obj3 = new Post("KFC","10/12/2019","BUY 4PC HOT&CRISPY GET 4PC STRIPS",
                R.drawable.oferta1c,0);
        postList.add(obj3);

        Post obj4 = new Post("ADIDAS ","23/08/2019","ADIDAS OFRON ZBRITJE DERI 70% PER ARSYE TE PERVJETORIT",
                R.drawable.oferta2a,0);
        postList.add(obj4);

        Post obj5 = new Post("ADIDAS ","28/09/2019","ADIDAS OFRON ZBRITJE DERI 70% PER ARSYE TE PERVJETORIT",
                R.drawable.oferta2b,0);
        postList.add(obj5);

        Post obj6 = new Post("ADIDAS ","11/10/2019","ADIDAS OFRON ZBRITJE DERI 70% PER ARSYE TE PERVJETORIT",
                R.drawable.oferta2c,0);
        postList.add(obj6);

        return postList;
    }
}
