package arianit.com.clickandgo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import arianit.com.clickandgo.R;
import arianit.com.clickandgo.adapters.PostAdapter;
import arianit.com.clickandgo.model.Post;

public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Post> postList = new ArrayList<>();
    PostAdapter adapter;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        recyclerView = rootView.findViewById(R.id.recyclerview_post);

        adapter = new PostAdapter(getContext());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setPost(listPosts());

        return rootView;

    }

    public List<Post> listPosts() {

        postList.clear();

        Post obj1 = new Post("Hotel Bushati","30/11/2019","5 Euro Nata",
                R.drawable.image_sample,R.drawable.logo_sample);
        postList.add(obj1);

        Post obj2 = new Post("LC WAIKIKI","30/11/2019","KOLEKCIONI I RI PRANVEROR DHE VEROR NE TE GJITHA PIKAT E LC WAIKIKI",
                R.drawable.news1b,0);
        postList.add(obj2);

        Post obj3 = new Post("LC WAIKIKI","30/11/2019","KOLEKCIONI I RI PRANVEROR DHE VEROR NE TE GJITHA PIKAT E LC WAIKIKI",
                R.drawable.news1c,0);
        postList.add(obj3);

        Post obj4 = new Post("ADIDAS ","30/11/2019","ADIDAS OFRON ZBRITJE DERI 70% PER ARSYE TE PERVJETORIT",
                R.drawable.oferta2a,0);
        postList.add(obj4);

        Post obj5 = new Post("ADIDAS ","30/11/2019","ADIDAS OFRON ZBRITJE DERI 70% PER ARSYE TE PERVJETORIT",
                R.drawable.oferta2b,0);
        postList.add(obj5);

        Post obj6 = new Post("ADIDAS ","30/11/2019","ADIDAS OFRON ZBRITJE DERI 70% PER ARSYE TE PERVJETORIT",
                R.drawable.oferta2c,0);
        postList.add(obj6);

        Post obj7 = new Post("01 BAU","30/11/2019","VEGLAT E REJA NGA ZVICRA DHE GJERMANIA",
                R.drawable.news2a,0);
        postList.add(obj7);

        Post obj8 = new Post("01 BAU","30/11/2019","VEGLAT E REJA NGA ZVICRA DHE GJERMANIA",
                R.drawable.news2b,0);
        postList.add(obj8);



        return postList;
    }
}
